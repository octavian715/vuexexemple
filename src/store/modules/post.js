export default {
  actions: {
    async fetchPosts (ctx, limit = 3) {
      const res = await fetch(`https://jsonplaceholder.typicode.com/posts?_limit=${limit}`)
      const posts = await res.json()
      ctx.commit('updatePosts', posts)
    }
  },
  state: {
    posts: []
  },
  getters: {
    validPost: state => state.posts.filter(p => p.title && p.body),
    allPosts: state => state.posts || [],
    postsCount: (state, getters) => getters.validPost.length
  },
  mutations: {
    updatePosts (state, posts) {
      state.posts = posts
    },
    createPost (state, newPost) {
      state.posts.unshift(newPost)
    }
  }
}
